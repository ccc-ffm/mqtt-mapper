FROM python:3.6-alpine

MAINTAINER Igor Scheller <igor.scheller@igorshp.de>

WORKDIR /app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .
COPY config.example.yml /config/mqtt-mapper.yml

VOLUME [ "/config" ]

CMD [ "python3", "./mqtt-mapper.py", "/config/mqtt-mapper.yml" ]
