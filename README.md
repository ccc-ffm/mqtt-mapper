# MQTT Mapper

The MQTT Mapper is a daemon that transparently maps between MQTT topics.

This makes it possible to build a topic structure to your personal liking.

## Example

We have a device exposing itself as follows:

- state topic: `home/power/abcdef/socket/state`
- command topic: `home/power/abcdef/socket/set`

In the path you can see the suffix of the devices MAC address, so you can 
tell your devices apart, but that is nothing that is really good to remember.

So let's give it a proper name to remember it by:

```yaml

---
realm: home
broker:
  host: mqtt.broker.domain
  port: 1883

mapping:
  # forward mapping
  lounge/leds/set:
    - power/abcdef/socket/set

  # reverse mapping
  power/abcdef/socket/state:
    - lounge/leds/state
```

Now when `home/lounge/leds/set` is called, your request is being forwarded to
`home/power/abcdef/socket/set`. In turn when `home/power/abcdef/socket/state` changes,
the transition is also applied to `home/lounge/leds/state`.

## Docker
To run the daemon in a docker container:

```bash
docker build -t="mqtt-mapper" .
docker run -it --rm \
  -v "${PWD}/config.yml":/config/mqtt-mapper.yml \
  mqtt-mapper
```
