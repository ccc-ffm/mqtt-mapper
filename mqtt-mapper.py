#!/usr/bin/env python3

import jsonschema
import yaml
import paho.mqtt.client as paho
import argparse
import logging
import sys
import signal
import re

logger = logging.getLogger(__name__)


def parse_config(fh):
    raw = yaml.load(fh)

    broker_schema = {
        'type': 'object',
        'properties': {
            'host': {'type': 'string'},
            'port': {'type': 'integer'}
        },
        'additionalProperties': False,
    }

    map_schema = {
        'type': 'object',
        'properties': {'/': {}},
        'patternProperties': {
            '^([a-zA-Z\\/\\.0-9_-]+)+$': {"$ref": "#/definitions/action"}
        },
        'additionalProperties': False,
    }

    action_schema = {
        'type': 'array',
    }

    schema = {
        'type': 'object',
        'properties': {
            'realm': {'type': 'string'},
            'broker': {'$ref': '#/definitions/broker'},
            'mapping': {'$ref': '#/definitions/mapping'},
        },
        'additionalProperties': False,
        'definitions': {
            'mapping': map_schema,
            'action': action_schema,
            'broker': broker_schema
        }
    }

    jsonschema.validate(raw, schema)

    return raw


class MqttMapper:
    def __init__(self, config):
        self.config = config
        self.connection = paho.Mosquitto()
        self.connection.on_message = self.on_message
        self.connection.on_connect = self.on_connect
        self.connection.enable_logger(logger)

        self.connection.connect(self.config['broker']['host'], self.config['broker']['port'])

    def run(self):
        logger.info('Starting MQTT-Loop')
        self.connection.loop_forever()

    def __mk_path(self, *args):
        return '/'.join([*[self.config['realm']] + list(args)])

    def on_connect(self, connection, data, flags, rc):
        realm = self.config['realm']
        for name in self.config['mapping'].keys():
            topic = '/'.join([realm, name])
            logger.info('Subscribing to: %s', topic)
            self.connection.subscribe(topic, 0)

    def on_message(self, conn, obj, msg):
        if not msg.topic.startswith(self.config['realm']):
            return

        local = re.sub('^/?{}'.format(re.escape(self.config['realm'])), '', msg.topic, 1)
        feature_name = local.lstrip('/')
        logger.debug("Feature triggered: %s", feature_name)
        try:
            subscribers = self.config['mapping'][feature_name]
            for subscriber in subscribers:
                topic = self.__mk_path(subscriber)
                logger.debug('Publishing (%s, %s)', topic, msg.payload)
                self.connection.publish(
                    topic, msg.payload.decode('latin-1'), retain=msg.retain
                )
        except KeyError as ex:
            logger.exception(ex)
            return

    def on_term(self, signal, frame):
        logger.warning('Reviced SIGTERM. Disconnecting from mqtt broker')
        self.connection.disconnect()
        sys.exit(0)


def main():
    parser = argparse.ArgumentParser()
    loglevel = {
        'DEBUG': logging.DEBUG,
        'INFO': logging.INFO,
        'ERROR': logging.ERROR
    }
    parser.add_argument(
        'config',
        type=argparse.FileType('r')
    )
    parser.add_argument(
        '--log-level',
        choices=loglevel.keys(),
        default='ERROR'
    )
    args = parser.parse_args()

    logging.basicConfig(level=loglevel[args.log_level])

    config = parse_config(args.config)
    print(config)

    mapper = MqttMapper(config)
    signal.signal(signal.SIGTERM, mapper.on_term)
    mapper.run()


if __name__ == "__main__":
    main()
